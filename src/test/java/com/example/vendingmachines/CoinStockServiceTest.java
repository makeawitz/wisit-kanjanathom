package com.example.vendingmachines;

import com.example.vendingmachines.constants.DefaultCoin;
import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.entities.CoinStock;
import com.example.vendingmachines.repositories.CoinStockRepository;
import com.example.vendingmachines.services.CoinService;
import com.example.vendingmachines.services.CoinStockService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CoinStockService.class })
public class CoinStockServiceTest {

	@Autowired
	private CoinStockService coinStockService;

	@MockBean
	private CoinService coinService;

	@MockBean
	private CoinStockRepository coinStockRepository;

	private final Map<DefaultCoin, CoinDto> validCoins = new HashMap<>();
	private final List<CoinStock> stocks = new ArrayList<>();

	private long nickelId;
	private long dimeId;
	private long quarterId;

	@Before
	public void before() {
		long count = 0;
		for (DefaultCoin dc : DefaultCoin.values()) {
			count++;
			CoinDto coin = Mockito.mock(CoinDto.class);
			Mockito.when(coin.getId()).thenReturn(count);
			Mockito.when(coin.getCoinName()).thenReturn(dc.getCoinName());
			Mockito.when(coin.getCoinValue()).thenReturn(dc.getCoinValue());
			Mockito.when(coin.getWeight()).thenReturn(dc.getWeight());
			Mockito.when(coin.getDiameter()).thenReturn(dc.getDiameter());
			Mockito.when(coin.getThickness()).thenReturn(dc.getThickness());
			Mockito.when(coin.getWeightErrorMargin()).thenReturn(dc.getWeightErrorMargin());
			Mockito.when(coin.getThicknessErrorMargin()).thenReturn(dc.getThicknessErrorMargin());
			validCoins.put(dc, coin);

			switch (dc) {
				case NICKEL:
					nickelId = count;
					break;
				case DIME:
					dimeId = count;
					break;
				case QUARTER:
					quarterId = count;
					break;
			}
		}

	}

	@Test
	public void testHasEnoughChange() {
		// Nickel x 3 = 1.5
		// Dime x 3 = 0.3
		// Quarter x 3 = 0.03
		for (CoinDto coin : validCoins.values().stream().sorted(Comparator.comparing(CoinDto::getCoinValue).reversed())
				.collect(Collectors.toList())) {
			stocks.add(new CoinStock(null, coin.getId(), 3));
		}

		Mockito.when(coinStockRepository.findAll()).thenReturn(stocks);
		Mockito.when(coinService.getCoinById(nickelId)).thenReturn(validCoins.get(DefaultCoin.NICKEL));
		Mockito.when(coinService.getCoinById(dimeId)).thenReturn(validCoins.get(DefaultCoin.DIME));
		Mockito.when(coinService.getCoinById(quarterId)).thenReturn(validCoins.get(DefaultCoin.QUARTER));

		Assert.assertTrue(coinStockService.hasEnoughChange(new BigDecimal("0.12")));
		Assert.assertTrue(coinStockService.hasEnoughChange(new BigDecimal("0.33")));
		Assert.assertTrue(coinStockService.hasEnoughChange(new BigDecimal("0.82")));

		Assert.assertFalse(coinStockService.hasEnoughChange(new BigDecimal("0.06")));
		Assert.assertFalse(coinStockService.hasEnoughChange(new BigDecimal("0.43")));
		Assert.assertFalse(coinStockService.hasEnoughChange(new BigDecimal("0.91")));

	}

	@Test
	public void testGetChangeList() {
		// Nickel x 3 = 1.5
		// Dime x 3 = 0.3
		// Quarter x 3 = 0.03
		for (CoinDto coin : validCoins.values().stream().sorted(Comparator.comparing(CoinDto::getCoinValue).reversed())
				.collect(Collectors.toList())) {
			stocks.add(new CoinStock(null, coin.getId(), 3));
		}

		Mockito.when(coinStockRepository.getSortedStock()).thenReturn(stocks);
		Mockito.when(coinService.getCoinById(nickelId)).thenReturn(validCoins.get(DefaultCoin.NICKEL));
		Mockito.when(coinService.getCoinById(dimeId)).thenReturn(validCoins.get(DefaultCoin.DIME));
		Mockito.when(coinService.getCoinById(quarterId)).thenReturn(validCoins.get(DefaultCoin.QUARTER));

		List<CoinDto> result = coinStockService.getChangeList(new BigDecimal("0.12"));
		Assert.assertEquals(1,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.DIME.getCoinName())).count());
		Assert.assertEquals(2,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.QUARTER.getCoinName())).count());

	}

	@Test
	public void testGetChangeList2() {
		// Nickel x 3 = 1.5
		// Dime x 3 = 0.3
		// Quarter x 3 = 0.03
		for (CoinDto coin : validCoins.values().stream().sorted(Comparator.comparing(CoinDto::getCoinValue).reversed())
				.collect(Collectors.toList())) {
			stocks.add(new CoinStock(null, coin.getId(), 3));
		}

		Mockito.when(coinStockRepository.getSortedStock()).thenReturn(stocks);
		Mockito.when(coinService.getCoinById(nickelId)).thenReturn(validCoins.get(DefaultCoin.NICKEL));
		Mockito.when(coinService.getCoinById(dimeId)).thenReturn(validCoins.get(DefaultCoin.DIME));
		Mockito.when(coinService.getCoinById(quarterId)).thenReturn(validCoins.get(DefaultCoin.QUARTER));

		List<CoinDto> result = coinStockService.getChangeList(new BigDecimal("0.33"));
		Assert.assertEquals(3,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.DIME.getCoinName())).count());
		Assert.assertEquals(3,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.QUARTER.getCoinName())).count());

	}

	@Test
	public void testGetChangeList3() {
		// Nickel x 3 = 1.5
		// Dime x 3 = 0.3
		// Quarter x 3 = 0.03
		for (CoinDto coin : validCoins.values().stream().sorted(Comparator.comparing(CoinDto::getCoinValue).reversed())
				.collect(Collectors.toList())) {
			stocks.add(new CoinStock(null, coin.getId(), 3));
		}

		Mockito.when(coinStockRepository.getSortedStock()).thenReturn(stocks);
		Mockito.when(coinService.getCoinById(nickelId)).thenReturn(validCoins.get(DefaultCoin.NICKEL));
		Mockito.when(coinService.getCoinById(dimeId)).thenReturn(validCoins.get(DefaultCoin.DIME));
		Mockito.when(coinService.getCoinById(quarterId)).thenReturn(validCoins.get(DefaultCoin.QUARTER));

		List<CoinDto> result = coinStockService.getChangeList(new BigDecimal("0.82"));
		Assert.assertEquals(1,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.NICKEL.getCoinName())).count());
		Assert.assertEquals(3,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.DIME.getCoinName())).count());
		Assert.assertEquals(2,
				result.stream().filter(c -> c.getCoinName().equals(DefaultCoin.QUARTER.getCoinName())).count());

	}

	@Test
	public void testAddInsertedCoinToStock() {
		// Nickel x 3 = 1.5
		// Dime x 3 = 0.3
		// Quarter x 3 = 0.03
		for (CoinDto coin : validCoins.values().stream().sorted(Comparator.comparing(CoinDto::getCoinValue).reversed())
				.collect(Collectors.toList())) {
			stocks.add(new CoinStock(null, coin.getId(), 3));
		}

		Mockito.when(coinStockRepository.findAll()).thenReturn(stocks);
		CoinDto nickel = validCoins.get(DefaultCoin.NICKEL);
		CoinDto dime = validCoins.get(DefaultCoin.DIME);
		CoinDto quarter = validCoins.get(DefaultCoin.QUARTER);

		List<CoinDto> list = Arrays.asList(quarter, dime, quarter, nickel);
		coinStockService.addInsertedCoinToStock(list);
		Mockito.verify(coinStockRepository, Mockito.times(1)).save(
				Mockito.argThat(c -> c.getCoinId().equals(quarterId) && c.getRemain() == 5));
		Mockito.verify(coinStockRepository, Mockito.times(1)).save(
				Mockito.argThat(c -> c.getCoinId().equals(dimeId) && c.getRemain() == 4));
		Mockito.verify(coinStockRepository, Mockito.times(1)).save(
				Mockito.argThat(c -> c.getCoinId().equals(nickelId) && c.getRemain() == 4));
	}

}
