package com.example.vendingmachines;

import com.example.vendingmachines.constants.DefaultCoin;
import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.dtos.InsertedCoinDto;
import com.example.vendingmachines.entities.Coin;
import com.example.vendingmachines.mappers.CoinMapper;
import com.example.vendingmachines.services.CoinCheckService;
import com.example.vendingmachines.services.CoinService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CoinCheckService.class, CoinMapper.class })
public class CoinCheckServiceTest {

	@Autowired
	private CoinCheckService coinCheckService;

	@Autowired
	private CoinMapper coinMapper;

	@MockBean
	private CoinService coinService;

	private final List<CoinDto> validCoins = new ArrayList<>();

	@Before
	public void before() {
		for (DefaultCoin dc : DefaultCoin.values()) {
			validCoins.add(coinMapper.mapToDto(
					new Coin(null, dc.getCoinName(), dc.getCoinValue(), dc.getWeight(), dc.getDiameter(),
							dc.getThickness(), dc.getWeightErrorMargin(), dc.getThicknessErrorMargin()),
					new CoinDto()));
		}
	}

	@Test
	public void testDetermineCoin_validCoin() {
		List<InsertedCoinDto> list = Arrays.asList(new InsertedCoinDto(302f, 15f, 6f),
				new InsertedCoinDto(290f, 15f, 6f), new InsertedCoinDto(95f, 12f, 4f),
				new InsertedCoinDto(196f, 10f, 3.9f), new InsertedCoinDto(97.5f, 5f, 2.42f),
				new InsertedCoinDto(100f, 5f, 2.51f), new InsertedCoinDto(107f, 5f, 1.99f));

		Mockito.when(coinService.getAllCoins()).thenReturn(validCoins);

		List<CoinDto> result = new ArrayList<>();
		for (InsertedCoinDto ic : list) {
			result.add(coinCheckService.determineCoin(ic));
		}

		// Has 3 valid coins
		Assert.assertEquals(3, result.stream().filter(c -> c.getCoinValue().signum() > 0).count());
		// Has 4 invalid coins
		Assert.assertEquals(4, result.stream().filter(c -> c.getCoinValue().signum() == 0).count());

		Assert.assertEquals(DefaultCoin.NICKEL.getCoinName(), result.get(0).getCoinName());
		Assert.assertEquals(DefaultCoin.NICKEL.getCoinValue(), result.get(0).getCoinValue());

		Assert.assertEquals(DefaultCoin.DIME.getCoinName(), result.get(3).getCoinName());
		Assert.assertEquals(DefaultCoin.DIME.getCoinValue(), result.get(3).getCoinValue());

		Assert.assertEquals(DefaultCoin.QUARTER.getCoinName(), result.get(4).getCoinName());
		Assert.assertEquals(DefaultCoin.QUARTER.getCoinValue(), result.get(4).getCoinValue());

	}

}
