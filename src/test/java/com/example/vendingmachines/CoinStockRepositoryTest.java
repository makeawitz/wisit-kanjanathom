package com.example.vendingmachines;

import com.example.vendingmachines.constants.DefaultCoin;
import com.example.vendingmachines.entities.Coin;
import com.example.vendingmachines.entities.CoinStock;
import com.example.vendingmachines.repositories.CoinRepository;
import com.example.vendingmachines.repositories.CoinStockRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class CoinStockRepositoryTest {

	@Autowired
	private CoinRepository coinRepository;

	@Autowired
	private CoinStockRepository coinStockRepository;

	@Value("${coin.stock.nickel}")
	private int nickelStock;

	@Value("${coin.stock.dime}")
	private int dimeStock;

	@Value("${coin.stock.quarter}")
	private int quarterStock;

	@Before
	public void before() {
		List<String> coins = coinRepository.findAllCoinName();
		for (DefaultCoin dc : DefaultCoin.values()) {
			if (!coins.contains(dc.getCoinName())) {
				coinRepository.save(
						new Coin(null, dc.getCoinName(), dc.getCoinValue(), dc.getWeight(), dc.getDiameter(),
								dc.getThickness(), dc.getWeightErrorMargin(), dc.getThicknessErrorMargin()));
			}
		}

		List<Coin> list = coinRepository.findAll().stream().sorted(Comparator.comparing(Coin::getCoinValue)).collect(
				Collectors.toList());
		List<Long> coinIdList = coinStockRepository.findAllCoinId();
		for (Coin coin : list) {
			if (coinIdList.contains(coin.getId())) {
				continue;
			}
			CoinStock stock = new CoinStock();
			if (coin.getCoinName().equals(DefaultCoin.NICKEL.getCoinName())) {
				stock.setCoinId(coin.getId());
				stock.setRemain(nickelStock);
				coinStockRepository.save(stock);
				continue;
			}
			if (coin.getCoinName().equals(DefaultCoin.DIME.getCoinName())) {
				stock.setCoinId(coin.getId());
				stock.setRemain(dimeStock);
				coinStockRepository.save(stock);
				continue;
			}
			if (coin.getCoinName().equals(DefaultCoin.QUARTER.getCoinName())) {
				stock.setCoinId(coin.getId());
				stock.setRemain(quarterStock);
				coinStockRepository.save(stock);
			}
		}
	}

	@Test
	public void testSortedStock() {
		List<CoinStock> stocks = coinStockRepository.getSortedStock();
		List<Coin> list = coinRepository.findAll().stream().sorted(Comparator.comparing(Coin::getCoinValue)).collect(
				Collectors.toList());
		for (int i = 0; i < stocks.size(); i++) {
			Assert.assertEquals(list.get(i).getId(), stocks.get(i).getCoinId());
		}
	}
}
