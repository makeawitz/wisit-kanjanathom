package com.example.vendingmachines;

import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.dtos.InsertedCoinDto;
import com.example.vendingmachines.dtos.ProductDto;
import com.example.vendingmachines.dtos.VendingMachineState;
import com.example.vendingmachines.services.VendingMachine;
import com.example.vendingmachines.utils.ResourceFileReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class VendingmachinesApplicationTests {

	@Autowired
	private VendingMachine vendingMachine;

	@Test
	void contextLoads() throws IOException, JSONException {
		JSONArray scenarios = ResourceFileReader.readScenario();

		System.out.println("*************");
		System.out.println("*** START ***");
		System.out.println("*************");
		for (int i = 0; i < scenarios.length(); i++) {
			JSONObject scenario = scenarios.getJSONObject(i);
			Action action = Action.toAction(scenario.getString("action"));

			VendingMachineState state;
			switch (action) {
				case INSERT_COIN:
					InsertedCoinDto dto = new InsertedCoinDto();
					dto.setWeight(Float.parseFloat(scenario.getString("weight")));
					dto.setDiameter(Float.parseFloat(scenario.getString("diameter")));
					dto.setThickness(Float.parseFloat(scenario.getString("thickness")));
					state = vendingMachine.insertCoinProxy(dto);
					break;
				case SELECT_PRODUCT:
					Long productId = scenario.getLong("productId");
					state = vendingMachine.selectProductProxy(productId);
					break;
				case RETURN_COIN:
					state = vendingMachine.returnCoinProxy();
					break;
				case SHOW:
					state = vendingMachine.getState();
					break;
				default:
					state = null;
					break;
			}
			if (state != null) {
				System.out.println("*** Action: " + action + " ***");

				System.out.println("All products: [");
				for (ProductDto product : state.getAllProducts()) {
					System.out.println(
							"id=" + product.getId() + " name=" + product.getName() + " price=" + product.getPrice());
				}
				System.out.println("]");
				System.out.println("Available products: [");
				for (ProductDto product : state.getAvailableProductList()) {
					System.out.println(
							"id=" + product.getId() + " name=" + product.getName() + " price=" + product.getPrice());
				}
				System.out.println("]");
				System.out.println("Out of stock products: [");
				for (ProductDto product : state.getOutOfStockList()) {
					System.out.println(
							"id=" + product.getId() + " name=" + product.getName() + " price=" + product.getPrice());
				}
				System.out.println("]");
				System.out.println("Display: " + state.getDisplay().getText());
				System.out.println("Subsequent display: " + state.getSubDisplay().getText());
				System.out.println("Current inserted coins: [");
				for (CoinDto coin : state.getCoinList()) {
					System.out.println(coin.getCoinName());
				}
				System.out.println("]");
				System.out.println("Return coins: [");
				for (CoinDto coin : state.getReturnCoinList()) {
					System.out.println(coin.getCoinName());
				}
				System.out.println("]");
				System.out.println("Complete: " + state.isFinish());
			}
			System.out.println("------------------------");
		}
		System.out.println("**************");
		System.out.println("*** FINISH ***");
		System.out.println("**************");

	}

	private enum Action {
		INSERT_COIN, SELECT_PRODUCT, RETURN_COIN, SHOW, FINISH;

		static Action toAction(String a) {
			for (Action action : values()) {
				if (action.name().equals(a)) {
					return action;
				}
			}
			return FINISH;
		}
	}
}
