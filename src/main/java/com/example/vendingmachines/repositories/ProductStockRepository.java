package com.example.vendingmachines.repositories;

import com.example.vendingmachines.entities.Product;
import com.example.vendingmachines.entities.ProductStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductStockRepository extends JpaRepository<ProductStock, Long> {

	// @formatter:off
	@Query("select p from Product p inner join ProductStock ps on p.id = ps.productId where ps.remain <= 0")
	// @formatter:on
	List<Product> getOutOfStock();

	// @formatter:off
	@Query("select p from Product p inner join ProductStock ps on p.id = ps.productId where ps.remain > 0")
	// @formatter:on
	List<Product> getAvailable();

	// @formatter:off
	@Query("select ps.productId from ProductStock ps")
	// @formatter:on
	List<Long> findAllProductId();

	Optional<ProductStock> findByProductId(Long id);
}
