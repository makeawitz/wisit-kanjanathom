package com.example.vendingmachines.repositories;

import com.example.vendingmachines.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

	// @formatter:off
	@Query("select p.name from Product p")
	// @formatter:on
	List<String> findAllProductName();

}
