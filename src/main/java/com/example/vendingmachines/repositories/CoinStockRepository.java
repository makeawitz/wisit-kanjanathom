package com.example.vendingmachines.repositories;

import com.example.vendingmachines.entities.CoinStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CoinStockRepository extends JpaRepository<CoinStock, Long> {

	// @formatter:off
	@Query("select cs.coinId from CoinStock cs")
	// @formatter:on
	List<Long> findAllCoinId();

	// @formatter:off
	@Query("select cs from CoinStock cs inner join Coin c on cs.coinId = c.id order by c.coinValue")
	// @formatter:on
	List<CoinStock> getSortedStock();

}
