package com.example.vendingmachines.repositories;

import com.example.vendingmachines.entities.Coin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CoinRepository extends JpaRepository<Coin, Long> {

	// @formatter:off
	@Query("select c.coinName from Coin c")
	// @formatter:on
	List<String> findAllCoinName();

	// @formatter:off
	@Query("select c from Coin c order by c.coinValue desc")
	// @formatter:on
	Page<Coin> getLowestValueCoin(Pageable pageable);
}
