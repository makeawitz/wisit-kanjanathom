package com.example.vendingmachines.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Coin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@Column(nullable = false)
	private String coinName;

	@Column(nullable = false, precision = 15, scale = 2)
	private BigDecimal coinValue;

	@Column(nullable = false)
	private Float weight;

	@Column(nullable = false)
	private Float diameter;

	@Column(nullable = false)
	private Float thickness;

	@Column(nullable = false)
	private Float weightErrorMargin;

	@Column(nullable = false)
	private Float thicknessErrorMargin;

}
