package com.example.vendingmachines.listeners;

import com.example.vendingmachines.services.CoinService;
import com.example.vendingmachines.services.CoinStockService;
import com.example.vendingmachines.services.ProductService;
import com.example.vendingmachines.services.ProductStockService;
import com.example.vendingmachines.services.VendingMachine;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class OnApplicationReadyListener {

	private final CoinService coinService;

	private final CoinStockService coinStockService;

	private final ProductService productService;

	private final ProductStockService productStockService;

	private final VendingMachine vendingMachine;

	public OnApplicationReadyListener(CoinService coinService, ProductService productService,
			VendingMachine vendingMachine, ProductStockService productStockService, CoinStockService coinStockService) {
		this.coinService = coinService;
		this.productService = productService;
		this.vendingMachine = vendingMachine;
		this.productStockService = productStockService;
		this.coinStockService = coinStockService;
	}

	@EventListener(ApplicationReadyEvent.class)
	public void onReady() {
		coinService.initialize();
		coinStockService.initialize();
		productService.initialize();
		productStockService.initialize();
		vendingMachine.dispense(true);
	}

}
