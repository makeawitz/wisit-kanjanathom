package com.example.vendingmachines.controllers;

import com.example.vendingmachines.dtos.InsertedCoinDto;
import com.example.vendingmachines.dtos.VendingMachineState;
import com.example.vendingmachines.services.VendingMachine;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vm")
public class VendingMachineController {

	private final VendingMachine vendingMachine;

	public VendingMachineController(VendingMachine vendingMachine) {
		this.vendingMachine = vendingMachine;
	}

	@GetMapping("/state")
	public VendingMachineState getState() {
		return vendingMachine.getState();
	}

	@PostMapping("/insert-coin")
	public VendingMachineState insertCoin(@RequestBody InsertedCoinDto dto) {
		return vendingMachine.insertCoinProxy(dto);
	}

	@PostMapping("/select-product")
	public VendingMachineState selectProduct(@RequestParam Long productId) {
		return vendingMachine.selectProductProxy(productId);
	}

	@PostMapping("/return-coin")
	public VendingMachineState returnCoin() {
		return vendingMachine.returnCoinProxy();
	}

}
