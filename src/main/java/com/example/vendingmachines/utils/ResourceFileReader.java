package com.example.vendingmachines.utils;

import org.json.JSONArray;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ResourceFileReader {

	public static JSONArray readScenario() throws IOException {
		File file = ResourceUtils.getFile("classpath:scenario.txt");
		String input = new String(Files.readAllBytes(file.toPath()));
		input = input.replaceAll("\n", "");
		input = input.replaceAll(" ", "");
		return new JSONArray(input);
	}
}
