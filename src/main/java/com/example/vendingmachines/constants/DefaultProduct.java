package com.example.vendingmachines.constants;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public enum DefaultProduct {
	COLA("Cola", "1.00"),
	CHIPS("Chips", "0.50"),
	CANDY("Candy", "0.65");

	final String productName;
	final BigDecimal price;

	DefaultProduct(String productName, String price) {
		this.productName = productName;
		this.price = new BigDecimal(price);
	}

}
