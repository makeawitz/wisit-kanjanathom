package com.example.vendingmachines.constants;

public class Errors {
	public static final String NOT_FOUND = "Not found.";
	public static final String NOT_ENOUGH_COIN = "Inserted coin values exceed the price but do not have enough coin.";
	public static final String NO_COIN_WAS_SET = "No coin was set in the machine.";
}
