package com.example.vendingmachines.constants;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public enum DefaultCoin {
	NICKEL("Nickel", "0.5", 300, 15, 6),
	DIME("Dime", "0.1", 200, 10, 4),
	QUARTER("Quarter", "0.01", 100, 5, 2);

	final String coinName;
	final BigDecimal coinValue;
	final float weight;
	final float diameter;
	final float thickness;

	final float weightErrorMargin = 5;
	final float thicknessErrorMargin = 0.5f;

	DefaultCoin(String coinName, String coinValue, float weight, float diameter, float thickness) {
		this.coinName = coinName;
		this.coinValue = new BigDecimal(coinValue);
		this.weight = weight;
		this.diameter = diameter;
		this.thickness = thickness;
	}

}
