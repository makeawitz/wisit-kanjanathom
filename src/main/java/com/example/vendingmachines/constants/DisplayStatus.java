package com.example.vendingmachines.constants;

public class DisplayStatus {
	public static final String INSERT_COIN = "INSERT COIN";
	public static final String PRICE = "PRICE";
	public static final String THANK_YOU = "THANK YOU";
	public static final String SOLD_OUT = "SOLD OUT";
	public static final String EXACT_CHANGE_ONLY = "EXACT CHANGE ONLY";
}
