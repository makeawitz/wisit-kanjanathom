package com.example.vendingmachines.mappers;

import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class CoinMapper {

	public CoinDto mapToDto(Coin src, CoinDto target) {
		target.setId(src.getId());
		target.setCoinName(src.getCoinName());
		target.setCoinValue(src.getCoinValue());
		target.setDiameter(src.getDiameter());
		target.setWeight(src.getWeight());
		target.setThickness(src.getThickness());
		target.setThicknessErrorMargin(src.getThicknessErrorMargin());
		target.setWeightErrorMargin(src.getWeightErrorMargin());
		return target;
	}
}
