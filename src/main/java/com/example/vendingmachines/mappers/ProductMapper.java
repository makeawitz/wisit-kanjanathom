package com.example.vendingmachines.mappers;

import com.example.vendingmachines.dtos.ProductDto;
import com.example.vendingmachines.entities.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

	public ProductDto mapToDto(Product src, ProductDto target) {
		target.setId(src.getId());
		target.setName(src.getName());
		target.setPrice(src.getPrice());
		return target;
	}
}
