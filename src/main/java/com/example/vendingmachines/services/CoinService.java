package com.example.vendingmachines.services;

import com.example.vendingmachines.constants.DefaultCoin;
import com.example.vendingmachines.constants.Errors;
import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.entities.Coin;
import com.example.vendingmachines.mappers.CoinMapper;
import com.example.vendingmachines.repositories.CoinRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoinService {

	private final CoinRepository coinRepository;

	private final CoinMapper coinMapper;

	private final String cache = "coinCache";

	public CoinService(CoinRepository coinRepository, CoinMapper coinMapper) {
		this.coinRepository = coinRepository;
		this.coinMapper = coinMapper;
	}

	@CacheEvict(value = cache, allEntries = true)
	@Transactional
	public void initialize() {
		List<String> coins = coinRepository.findAllCoinName();
		for (DefaultCoin dc : DefaultCoin.values()) {
			if (!coins.contains(dc.getCoinName())) {
				coinRepository.save(
						new Coin(null, dc.getCoinName(), dc.getCoinValue(), dc.getWeight(), dc.getDiameter(),
								dc.getThickness(), dc.getWeightErrorMargin(), dc.getThicknessErrorMargin()));
			}
		}
	}

	@Cacheable(value = cache, key = "{ #root.methodName }", sync = true)
	public List<CoinDto> getAllCoins() {
		return coinRepository.findAll().stream().map(coin -> coinMapper.mapToDto(coin, new CoinDto())).collect(
				Collectors.toList());
	}

	@Cacheable(value = cache, key = "{ #root.methodName, #id }", sync = true)
	public CoinDto getCoinById(Long id) {
		return coinMapper.mapToDto(
				coinRepository.findById(id).orElseThrow(() -> new RuntimeException(Errors.NOT_FOUND)), new CoinDto());
	}

	@Cacheable(value = cache, key = "{ #root.methodName }", sync = true)
	public CoinDto getLowestValueCoin() {
		Page<Coin> page = coinRepository.getLowestValueCoin(PageRequest.of(0, 1));
		if (page.isEmpty()) {
			throw new IllegalStateException(Errors.NO_COIN_WAS_SET);
		}
		return coinMapper.mapToDto(page.getContent().get(0), new CoinDto());
	}
}
