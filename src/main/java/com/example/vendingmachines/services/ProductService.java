package com.example.vendingmachines.services;

import com.example.vendingmachines.constants.DefaultProduct;
import com.example.vendingmachines.constants.Errors;
import com.example.vendingmachines.dtos.ProductDto;
import com.example.vendingmachines.entities.Product;
import com.example.vendingmachines.mappers.ProductMapper;
import com.example.vendingmachines.repositories.ProductRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

	private final ProductRepository productRepository;

	private final ProductMapper productMapper;

	private final String cache = "productCache";

	public ProductService(ProductRepository productRepository, ProductMapper productMapper) {
		this.productRepository = productRepository;
		this.productMapper = productMapper;
	}

	@CacheEvict(value = cache, allEntries = true)
	@Transactional
	public void initialize() {
		List<String> products = productRepository.findAllProductName();
		for (DefaultProduct dp : DefaultProduct.values()) {
			if (!products.contains(dp.getProductName())) {
				productRepository.save(new Product(null, dp.getProductName(), dp.getPrice()));
			}
		}
	}

	@Cacheable(value = cache, key = "{ #root.methodName }", sync = true)
	public List<ProductDto> getAllProducts() {
		return productRepository.findAll().stream().map(product -> productMapper.mapToDto(product, new ProductDto()))
				.collect(Collectors.toList());
	}

	@Cacheable(value = cache, key = "{ #root.methodName, #id }", sync = true)
	public ProductDto getProductById(Long id) {
		return productMapper.mapToDto(
				productRepository.findById(id).orElseThrow(() -> new RuntimeException(Errors.NOT_FOUND)),
				new ProductDto());
	}

}
