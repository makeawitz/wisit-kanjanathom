package com.example.vendingmachines.services;

import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.dtos.InsertedCoinDto;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CoinCheckService {

	private final CoinService coinService;

	public CoinCheckService(CoinService coinService) {
		this.coinService = coinService;
	}

	public CoinDto determineCoin(InsertedCoinDto dto) {
		List<CoinDto> validCoins = coinService.getAllCoins();
		for (CoinDto coinDto : validCoins) {
			// Check validity of coin and find which type of coin is the inserted coin
			if (dto.getWeight() >= coinDto.getWeight() - coinDto.getWeightErrorMargin()
					&& dto.getWeight() <= coinDto.getWeight() + coinDto.getWeightErrorMargin()
					&& dto.getDiameter().equals(coinDto.getDiameter())
					&& dto.getThickness() >= coinDto.getThickness() - coinDto.getThicknessErrorMargin()
					&& dto.getThickness() <= coinDto.getThickness() + coinDto.getThicknessErrorMargin()) {
				return coinDto;
			}
		}

		// Coin is not valid
		CoinDto invalidCoin = new CoinDto();
		invalidCoin.setCoinName("Penny");
		invalidCoin.setCoinValue(BigDecimal.ZERO);
		return invalidCoin;
	}
}
