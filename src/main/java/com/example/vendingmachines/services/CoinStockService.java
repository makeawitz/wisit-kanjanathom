package com.example.vendingmachines.services;

import com.example.vendingmachines.constants.DefaultCoin;
import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.entities.CoinStock;
import com.example.vendingmachines.repositories.CoinStockRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CoinStockService {

	private final CoinStockRepository coinStockRepository;

	private final CoinService coinService;

	@Value("${coin.stock.nickel}")
	private int nickelStock;

	@Value("${coin.stock.dime}")
	private int dimeStock;

	@Value("${coin.stock.quarter}")
	private int quarterStock;

	public CoinStockService(CoinStockRepository coinStockRepository, CoinService coinService) {
		this.coinStockRepository = coinStockRepository;
		this.coinService = coinService;
	}

	@Transactional
	public void initialize() {
		List<CoinDto> list = coinService.getAllCoins();
		List<Long> coinIdList = coinStockRepository.findAllCoinId();
		for (CoinDto coin : list) {
			if (coinIdList.contains(coin.getId())) {
				continue;
			}
			CoinStock stock = new CoinStock();
			if (coin.getCoinName().equals(DefaultCoin.NICKEL.getCoinName())) {
				stock.setCoinId(coin.getId());
				stock.setRemain(nickelStock);
				coinStockRepository.save(stock);
				continue;
			}
			if (coin.getCoinName().equals(DefaultCoin.DIME.getCoinName())) {
				stock.setCoinId(coin.getId());
				stock.setRemain(dimeStock);
				coinStockRepository.save(stock);
				continue;
			}
			if (coin.getCoinName().equals(DefaultCoin.QUARTER.getCoinName())) {
				stock.setCoinId(coin.getId());
				stock.setRemain(quarterStock);
				coinStockRepository.save(stock);
			}
		}
	}

	public boolean hasEnoughChange(BigDecimal change) {
		if (change.signum() == 0) {
			return true;
		}
		List<CoinStock> stock = coinStockRepository.findAll();

		for (CoinStock s : stock) {
			CoinDto coin = coinService.getCoinById(s.getCoinId());
			BigDecimal remain = new BigDecimal(s.getRemain());
			BigDecimal totalForCoin = remain.multiply(coin.getCoinValue());

			if (totalForCoin.compareTo(change) < 0) {
				change = change.subtract(totalForCoin);
				continue;
			}

			if (change.remainder(coin.getCoinValue()).signum() == 0) {
				return true;
			}
			change = change.remainder(coin.getCoinValue());
		}
		return false;
	}

	@Transactional
	public List<CoinDto> getChangeList(BigDecimal change) {
		List<CoinStock> stock = coinStockRepository.getSortedStock();
		List<CoinDto> result = new ArrayList<>();

		for (CoinStock s : stock) {
			CoinDto coin = coinService.getCoinById(s.getCoinId());
			BigDecimal remain = new BigDecimal(s.getRemain());

			int count = 0;
			int amount = change.divide(coin.getCoinValue(), 0, RoundingMode.DOWN).intValue();
			for (; count < amount && count < remain.intValue(); count++) {
				result.add(coin);
			}

			s.setRemain(s.getRemain() - count);
			coinStockRepository.save(s);
			if (change.remainder(coin.getCoinValue()).signum() == 0) {
				return result;
			}
			change = change.remainder(coin.getCoinValue());
		}
		return Collections.emptyList();
	}

	@Transactional
	public void addInsertedCoinToStock(List<CoinDto> list) {
		if (list.isEmpty()) {
			return;
		}

		Map<Long, Integer> map = new HashMap<>();
		for (CoinDto coin : list) {
			map.put(coin.getId(), map.getOrDefault(coin.getId(), 0) + 1);
		}

		List<CoinStock> stock = coinStockRepository.findAll();
		for (CoinStock cs : stock) {
			if (map.containsKey(cs.getCoinId())) {
				cs.setRemain(cs.getRemain() + map.get(cs.getCoinId()));
				coinStockRepository.save(cs);
			}
		}
	}
}
