package com.example.vendingmachines.services;

import com.example.vendingmachines.constants.DefaultProduct;
import com.example.vendingmachines.constants.Errors;
import com.example.vendingmachines.dtos.ProductDto;
import com.example.vendingmachines.entities.ProductStock;
import com.example.vendingmachines.mappers.ProductMapper;
import com.example.vendingmachines.repositories.ProductStockRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductStockService {

	private final ProductService productService;

	private final ProductStockRepository productStockRepository;

	private final ProductMapper productMapper;

	@Value("${product.stock.cola}")
	private int colaStock;

	@Value("${product.stock.chips}")
	private int chipsStock;

	@Value("${product.stock.candy}")
	private int candyStock;

	@Transactional
	public void initialize() {
		List<ProductDto> list = productService.getAllProducts();
		List<Long> productIdList = productStockRepository.findAllProductId();
		for (ProductDto product : list) {
			if (productIdList.contains(product.getId())) {
				continue;
			}
			ProductStock stock = new ProductStock();
			if (product.getName().equals(DefaultProduct.COLA.getProductName())) {
				stock.setProductId(product.getId());
				stock.setRemain(colaStock);
				productStockRepository.save(stock);
				continue;
			}
			if (product.getName().equals(DefaultProduct.CHIPS.getProductName())) {
				stock.setProductId(product.getId());
				stock.setRemain(chipsStock);
				productStockRepository.save(stock);
				continue;
			}
			if (product.getName().equals(DefaultProduct.CANDY.getProductName())) {
				stock.setProductId(product.getId());
				stock.setRemain(candyStock);
				productStockRepository.save(stock);
			}
		}
	}

	public ProductStockService(ProductStockRepository productStockRepository, ProductMapper productMapper,
			ProductService productService) {
		this.productStockRepository = productStockRepository;
		this.productMapper = productMapper;
		this.productService = productService;
	}

	public int getRemainStock(Long productId) {
		return productStockRepository.findById(productId).orElseThrow(() -> new RuntimeException(Errors.NOT_FOUND))
				.getRemain();
	}

	public List<ProductDto> getOutOfStock() {
		return productStockRepository.getOutOfStock().stream().map(p -> productMapper.mapToDto(p, new ProductDto()))
				.collect(Collectors.toList());
	}

	public List<ProductDto> getAvailable() {
		return productStockRepository.getAvailable().stream().map(p -> productMapper.mapToDto(p, new ProductDto()))
				.collect(Collectors.toList());
	}

	@Transactional
	public void decreaseStock(Long id) {
		ProductStock productStock = productStockRepository.findByProductId(id).orElseThrow(
				() -> new IllegalStateException(Errors.NOT_FOUND));
		System.err.println("** stock: "+productStock.getRemain());
		productStock.setRemain(productStock.getRemain() - 1);
		productStockRepository.save(productStock);
	}
}
