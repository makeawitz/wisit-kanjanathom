package com.example.vendingmachines.services;

import com.example.vendingmachines.constants.DisplayStatus;
import com.example.vendingmachines.constants.Errors;
import com.example.vendingmachines.dtos.CoinDto;
import com.example.vendingmachines.dtos.Display;
import com.example.vendingmachines.dtos.InsertedCoinDto;
import com.example.vendingmachines.dtos.VendingMachineState;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VendingMachine {

	private final CoinService coinService;

	private final CoinCheckService coinCheckService;

	private final CoinStockService coinStockService;

	private final ProductService productService;

	private final ProductStockService productStockService;

	private VendingMachineState state;

	public VendingMachine(CoinCheckService coinCheckService, ProductStockService productStockService,
			ProductService productService, CoinStockService coinStockService, CoinService coinService) {
		this.coinCheckService = coinCheckService;
		this.productStockService = productStockService;
		this.productService = productService;
		this.coinStockService = coinStockService;
		this.coinService = coinService;
	}

	public VendingMachineState getState() {
		state.getReturnCoinList().clear();
		BigDecimal total = calculateTotalInserted();
		if (total.signum() == 0) {
			state.setDisplay(new Display(DisplayStatus.INSERT_COIN, BigDecimal.ZERO));
			return state;
		}
		state.setDisplay(new Display("$" + total.setScale(2, RoundingMode.DOWN).toPlainString(), total));
		return state;
	}

	public VendingMachineState insertCoinProxy(InsertedCoinDto dto) {
		VendingMachineState result = insertCoin(dto);
		dispense(result.isFinish());
		return result;
	}

	private VendingMachineState insertCoin(InsertedCoinDto dto) {
		state.getReturnCoinList().clear();
		CoinDto coin = coinCheckService.determineCoin(dto);
		if (coin.getCoinValue().signum() == 0) {
			// Invalid coin inserted -> add to return coin list without update value
			state.getReturnCoinList().add(coin);
			return state;
		}

		state.getCoinList().add(coin);
		onCoinInserted();
		return state;
	}

	public VendingMachineState returnCoinProxy() {
		VendingMachineState result = returnCoin();
		dispense(result.isFinish());
		return result;
	}

	private VendingMachineState returnCoin() {
		state.getReturnCoinList().clear();
		state.getReturnCoinList().addAll(state.getCoinList());
		state.getCoinList().clear();
		state.setDisplay(new Display(DisplayStatus.INSERT_COIN, BigDecimal.ZERO));
		state.setFinish(true);
		return state;
	}

	public VendingMachineState selectProductProxy(Long productId) {
		VendingMachineState result = selectProduct(productId);
		dispense(result.isFinish());
		return result;
	}

	private VendingMachineState selectProduct(Long productId) {
		state.getReturnCoinList().clear();
		int remain = productStockService.getRemainStock(productId);
		if (remain <= 0) {
			state.setSelectedProduct(null);
			state.setDisplay(new Display(DisplayStatus.SOLD_OUT, BigDecimal.ZERO));
			return state;
		}

		state.setSelectedProduct(productService.getProductById(productId));
		onProductSelected();
		return state;
	}

	public void dispense(boolean finish) {
		if (finish) {
			state = new VendingMachineState();
			state.setAllProducts(productService.getAllProducts());
			state.setOutOfStockList(productStockService.getOutOfStock());
			state.setAvailableProductList(productStockService.getAvailable());
			state.setDisplay(new Display(DisplayStatus.INSERT_COIN, BigDecimal.ZERO));
			state.setSubDisplay(new Display("", BigDecimal.ZERO));
		}
	}

	private void onCoinInserted() {
		BigDecimal total = calculateTotalInserted();
		if (state.getSelectedProduct() == null) {
			state.setDisplay(new Display("$" + total.setScale(2, RoundingMode.DOWN).toPlainString(), total));
			return;
		}
		state.setSubDisplay(new Display("$" + total.setScale(2, RoundingMode.DOWN).toPlainString(), total));
		if (total.compareTo(state.getSelectedProduct().getPrice()) >= 0) {
			settleCoinAndPrice();
		}
	}

	private void onProductSelected() {
		Display display = state.getDisplay();
		BigDecimal estimateChange = state.getSelectedProduct().getPrice().subtract(
				coinService.getLowestValueCoin().getCoinValue());
		if (!coinStockService.hasEnoughChange(estimateChange)) {
			if (display.getValue().compareTo(state.getSelectedProduct().getPrice()) == 0) {
				settleCoinAndPrice();
				return;
			}
			state.getReturnCoinList().clear();
			state.getReturnCoinList().addAll(state.getCoinList());
			state.getCoinList().clear();
			state.setDisplay(new Display(DisplayStatus.EXACT_CHANGE_ONLY, BigDecimal.ZERO));
			return;
		}

		if (display.getValue().compareTo(state.getSelectedProduct().getPrice()) >= 0) {
			settleCoinAndPrice();
			return;
		}

		state.setSubDisplay(display);
		state.setDisplay(new Display(
				DisplayStatus.PRICE + " $" + state.getSelectedProduct().getPrice().setScale(2, RoundingMode.DOWN)
						.toPlainString(), BigDecimal.ZERO));
	}

	private void settleCoinAndPrice() {
		List<CoinDto> list = state.getCoinList().stream().sorted(Comparator.comparing(CoinDto::getCoinValue).reversed())
				.collect(Collectors.toList());
		List<CoinDto> settled = new ArrayList<>();
		BigDecimal remain = state.getSelectedProduct().getPrice();
		CoinDto tmp = null;
		for (CoinDto coin : list) {
			if (remain.compareTo(coin.getCoinValue()) < 0) {
				tmp = coin;
				continue;
			}
			remain = remain.subtract(coin.getCoinValue());

			settled.add(coin);

			if (remain.signum() == 0) {
				break;
			}
		}

		for (CoinDto coin : settled) {
			list.remove(coin);
		}

		// Exact insert
		if (remain.signum() == 0) {
			coinStockService.addInsertedCoinToStock(settled);
			settleAndCheckStock(list);
			return;
		}

		if (tmp == null) {
			throw new IllegalStateException(Errors.NOT_ENOUGH_COIN);
		}

		// Need to make change
		if (remain.signum() > 0) {
			settled.add(tmp);
			list.remove(tmp);
			list.addAll(coinStockService.getChangeList(tmp.getCoinValue().subtract(remain)));
			coinStockService.addInsertedCoinToStock(settled);
			settleAndCheckStock(list);
			return;
		}

		// Insert more than amount
		list.addAll(coinStockService.getChangeList(remain.abs()));
		settleAndCheckStock(list);
	}

	private void settleAndCheckStock(List<CoinDto> list) {
		state.getCoinList().clear();
		state.getReturnCoinList().addAll(list);
		state.setDisplay(new Display(DisplayStatus.THANK_YOU, BigDecimal.ZERO));
		state.setFinish(true);
		productStockService.decreaseStock(state.getSelectedProduct().getId());
		state.setOutOfStockList(productStockService.getOutOfStock());
	}

	private BigDecimal calculateTotalInserted() {
		BigDecimal result = BigDecimal.ZERO;
		for (CoinDto coin : state.getCoinList()) {
			result = result.add(coin.getCoinValue());
		}
		return result;
	}

}
