package com.example.vendingmachines;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class VendingmachinesApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendingmachinesApplication.class, args);
	}

}
