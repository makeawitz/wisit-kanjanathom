package com.example.vendingmachines.dtos;

import com.example.vendingmachines.constants.DisplayStatus;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class VendingMachineState {

	@Setter(AccessLevel.NONE)
	private List<CoinDto> coinList = new ArrayList<>();

	@Setter(AccessLevel.NONE)
	private List<CoinDto> returnCoinList = new ArrayList<>();

	private Display display = new Display(DisplayStatus.INSERT_COIN, BigDecimal.ZERO);

	private Display subDisplay;

	private List<ProductDto> allProducts;

	private List<ProductDto> availableProductList;

	private List<ProductDto> outOfStockList;

	private ProductDto selectedProduct;

	private boolean finish;
}
