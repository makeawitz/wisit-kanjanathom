package com.example.vendingmachines.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@Data
public class CoinDto {

	private Long id;

	private String coinName;

	private BigDecimal coinValue;

	private Float weight;

	private Float diameter;

	private Float thickness;

	private Float weightErrorMargin;

	private Float thicknessErrorMargin;

}
