package com.example.vendingmachines.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@Data
public class ProductDto {

	private Long id;

	private String name;

	private BigDecimal price;

}
