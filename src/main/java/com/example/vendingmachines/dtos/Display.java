package com.example.vendingmachines.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class Display {

	private String text;

	private BigDecimal value;

}
