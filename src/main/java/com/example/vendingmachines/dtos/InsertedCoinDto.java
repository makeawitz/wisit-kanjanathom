package com.example.vendingmachines.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class InsertedCoinDto {

	private Float weight;

	private Float diameter;

	private Float thickness;

}
